function homeSliderInit() {
  const homeSliderProps = {
    className: '.home_slider_section__',
    effect: "fade",
    slidesPerColumn: 1,
    spaceBetween: 0,
    speed: 3000,
    observer: true,
    observeParents: true,
    loop:true,
  }

  swiperInit(homeSliderProps)
}

function healthCareServicesSwiperInit() {
  let slidesNumber = $('.health_care_services_section .swiper-wrapper .swiper-slide').length
  const healthCareServicesSwiperBreakNormalPoints = {
    0: {
      slidesPerView: slidesNumber <= 2 ? slidesNumber : 2,
    },
    480: {
      slidesPerView: slidesNumber <= 2 ? slidesNumber : 2,
    },
    767: {
      slidesPerView: slidesNumber <= 2 ? slidesNumber : 2,
    },
    992: {
      slidesPerView: slidesNumber <= 3 ? slidesNumber : 3,
    },
    1200: {
      slidesPerView: slidesNumber <= 4 ? slidesNumber : 4,
    },
  };


  const healthCareServicesSliderSectionProps = {
    className: '.health_care_services_section',
    breakpoints: healthCareServicesSwiperBreakNormalPoints,
    spaceBetween: 30,
    autoplay: false,
    observer: true,
    observeParents: true,
  };

  swiperInit(healthCareServicesSliderSectionProps)
}

function ourPartnersSwiperInit() {
  let slidesNumber = $('.our_partners_section .swiper-wrapper .swiper-slide').length
  const healthCareServicesSwiperBreakNormalPoints = {
    0: {
      slidesPerView: slidesNumber <= 2 ? slidesNumber : 2,
    },
    480: {
      slidesPerView: slidesNumber <= 2 ? slidesNumber : 2,
    },
    767: {
      slidesPerView: slidesNumber <= 2 ? slidesNumber : 2,
    },
    992: {
      slidesPerView: slidesNumber <= 3 ? slidesNumber : 3,
    },
    1200: {
      slidesPerView: slidesNumber <= 4 ? slidesNumber : 4,
    },
  };

  const healthCareServicesSliderSectionProps = {
    className: '.our_partners_section',
    breakpoints: healthCareServicesSwiperBreakNormalPoints,
    spaceBetween: 30,
    autoplay: false,
    observer: true,
    observeParents: true,
  };

  swiperInit(healthCareServicesSliderSectionProps)
}

function customersSwiperInit() {
  let slidesNumber = $('.our_customers_opinions_section .swiper-wrapper .swiper-slide').length
  const healthCareServicesSwiperBreakNormalPoints = {
    0: {
      slidesPerView: slidesNumber <= 1 ? slidesNumber : 1,
    },
    480: {
      slidesPerView: slidesNumber <= 1 ? slidesNumber : 1,
    },
    767: {
      slidesPerView: slidesNumber <= 2 ? slidesNumber : 2,
    },
    992: {
      slidesPerView: slidesNumber <= 2 ? slidesNumber : 2,
    },
    1200: {
      slidesPerView: slidesNumber <= 2 ? slidesNumber : 2,
    },
  };

  const healthCareServicesSliderSectionProps = {
    className: '.our_customers_opinions_section',
    breakpoints: healthCareServicesSwiperBreakNormalPoints,
    spaceBetween: 30,
    autoplay: false,
    observer: true,
    observeParents: true,
  };

  swiperInit(healthCareServicesSliderSectionProps)
}